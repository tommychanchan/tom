# weather location: HK

# imports
import debug_utils as du
import os
import random
import json
from urllib.parse import quote
import urllib.request as req
import requests
import bs4
import pyperclip
import sys
import socket
import getpass
import wikipedia
import platform
import warnings

warnings.filterwarnings("ignore")


# global variables

VERSION = "1.0.0.1"
# <version>1.0.0.1</version>  # for "version" command

WINDOWS = platform.system() == "Windows"

tomFolder = ""
if WINDOWS:
    tomFolder = os.path.join(os.getenv("appdata"), "Tom")
else:
    tomFolder = os.path.join(os.path.expanduser("~"), "Tom")

NAME = "Tom"

# fmt: off
data = {
    "config": {
        "username": "Boss",
    },
    "private_config": {
        "hide_input": False,
    },
    "remember": {},
}
# fmt: on

DATA_FILE = os.path.join(tomFolder, "data.json")

ans = ""
para = []  # para[0]==command, para[1]==first parameter
RELEASE_LINK = "https://gitlab.com/tommychanchan/tom"
wikipedia.set_lang("zh")


if not os.path.exists(tomFolder):
    os.makedirs(tomFolder)


def readData():
    global data

    if os.path.exists(DATA_FILE):  # check if file exists
        with open(DATA_FILE, "r", encoding="utf-8") as f:
            dataJson = f.read()

            temp = json.loads(dataJson)
            for key in temp:
                data[key] = temp[key]


def writeData():
    with open(DATA_FILE, "w+", encoding="utf-8") as f:
        dataJson = json.dumps(data, sort_keys=True, indent=4, separators=(",", ": "))
        f.write(dataJson)


def convertTuple(tup):
    return "".join(tup)


def translate(msg, lang):
    if (msg == "") or (msg is None):
        return "What do you want to translate?"

    # msg=quote(msg)

    url = "http://translate.googleapis.com/translate_a/single"

    headers = {
        "User-Agent": "Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36 OPR/62.0.3331.116"
    }

    query = {"client": "gtx", "sl": "auto", "tl": lang, "dt": "t", "q": msg}

    result = requests.get(
        url, params=query, timeout=40, headers=headers, verify=False
    ).text

    if result == '[,,""]':
        return "No result"

    while ",," in result:
        result = result.replace(",,", ",null,")
        result = result.replace("[,", "[null,")

    data = json.loads(result)

    try:
        language = data[2]
    except:
        language = "?"

    re = "".join(x[0] for x in data[0]), language

    re = convertTuple(re)[:-2]

    re = re.encode().decode("UTF-8")

    if re[-3:] == "en-":
        re = re[:-3]
    elif re[-3:] == "zh-":
        re = re[:-3]

    pyperclip.copy(re)  # copy the translated string into clipboard

    return re


def translate2(msg):
    if (msg == "") or (msg is None):
        return ""

    # msg=quote(msg)

    url = "http://translate.googleapis.com/translate_a/single"

    headers = {
        "User-Agent": "Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36 OPR/62.0.3331.116"
    }

    query = {"client": "gtx", "sl": "zh-CN", "tl": "zh-TW", "dt": "t", "q": msg}

    result = requests.get(
        url, params=query, timeout=40, headers=headers, verify=False
    ).text

    if result == '[,,""]':
        return "No result"

    while ",," in result:
        result = result.replace(",,", ",null,")
        result = result.replace("[,", "[null,")

    data = json.loads(result)

    try:
        language = data[2]
    except:
        language = "?"

    re = "".join(x[0] for x in data[0]), language

    re = convertTuple(re)[:-2]

    re = re.encode().decode("UTF-8")

    if re[-3:] == "zh-":
        re = re[:-3]

    return re


def say(*args):
    print(f"{NAME}:", *args)


def parse(ans):
    para = " ".join(ans.split()).split(
        " "
    )  # para[0] == command, para[1] == first parameter
    if len(para) >= 1:
        para[0] = para[0].lower()

    return para


def weather():
    re = "\n\n"

    url = "http://rss.weather.gov.hk/rss/CurrentWeather.xml"

    headers = {
        "User-Agent": "Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36 OPR/62.0.3331.116"
    }

    result = requests.get(url, timeout=40, headers=headers, verify=True).text
    result = result.encode().decode("UTF-8")

    root = bs4.BeautifulSoup(result, "lxml")
    time = root.find_all("title")

    # last update
    re += f"Last update: {time[2].text[20:]}\n"

    description = root.find_all("description")
    description = bs4.BeautifulSoup(str(description[1]), "lxml")
    temp = description.find("p")

    start = int(str(temp).find("Air temperature")) + 18
    end = int(str(temp).find("degrees Celsius")) - 1

    # temperature
    re += f"Temperature: {str(temp)[start:end]} ℃\n"

    start = int(str(temp).find("Relative Humidity")) + 20
    end = int(str(temp).find("per cent")) - 1

    # Relative Humidity
    re += f"Relative Humidity: {str(temp)[start:end]}%"

    return re


def ip():
    re = "\n\n"

    # get internal ip address
    # fmt: off
    IPAddr = [l for l in ([ip for ip in socket.gethostbyname_ex(socket.gethostname())[2] if not ip.startswith("127.")][:1], [[(s.connect(('8.8.8.8', 53)), s.getsockname()[0], s.close()) for s in [socket.socket(socket.AF_INET, socket.SOCK_DGRAM)]][0][1]]) if l][0][0]
    # fmt: on

    re += f"Internal ip: {IPAddr}\n"

    # get external ip address
    ip = requests.get("https://api.ipify.org").text
    # xxx.xxx.xxx.xxx
    if len(ip) > 15:
        ip = requests.get("https://ip.seeip.org/").text
        if len(ip) > 15:
            re += "External ip: Could not get your external ip address."
        else:
            re += f"External ip: {ip}"
    else:
        re += f"External ip: {ip}"

    return re


def getCurrentVersion():
    url = "https://gitlab.com/tommychanchan/tom/-/raw/main/main.py"

    headers = {
        "User-Agent": "Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36 OPR/62.0.3331.116"
    }

    result = requests.get(url, timeout=40, headers=headers, verify=True).text
    result = result.encode().decode("UTF-8")

    root = bs4.BeautifulSoup(result, "lxml")
    ver = root.find("version")
    return str(ver.text)


def commandInfo(com=""):
    reArray = []
    re = "\n"
    if com == "":
        # all commands
        reArray.append(
            "chinese (x): To translate (x) to Chinese. (using google translate)"
        )
        reArray.append("clear: To clear the screen.")
        reArray.append('config [x]: To config [x]. Type "config" to know more.')
        reArray.append(
            "english (x): To translate (x) to English. (using google translate)"
        )
        reArray.append("help [x]: To show all command or show usage of command [x].")
        reArray.append("hide [on|off]: To toggle the input method.")
        reArray.append("ip: To show ip address.")
        reArray.append("lock: To lock the screen.")
        reArray.append("open (x): To open application (x) with parameter(s).")
        reArray.append(
            "random (x) (y): To generate a random number between [x] and [y] (inclusive)."
        )
        reArray.append("remember (x) (y): To link (x) and (y) (for opening app).")
        reArray.append("restart: To restart the computer.")
        reArray.append("run (x): To run application (x) with parameter(s) in Tom.")
        reArray.append("shutdown: To shutdown the computer.")
        reArray.append(
            "version: To see the version of the program and check for update."
        )
        reArray.append("weather: To show current weather information.")
        reArray.append(
            'where (x): To show the location of command (x) (linked by command "remember").'
        )
        reArray.append("wiki: To get information from wikipedia.")

        for line in reArray:
            re += f"\n{line}"
    else:
        # specific command
        command = {}
        command[
            "chinese"
        ] = 'To translate a text to Chinese. (using google translate)\nThen copy the result to clipboard.\nE.g. "chi how are you?"'
        command["clear"] = "To clear the screen."
        command[
            "config"
        ] = 'To config something.\nE.g. "config username test" to change username to "test".\nType "config" to know more.'
        command[
            "english"
        ] = 'To translate a text to English. (using google translate)\nThen copy the result to clipboard.\nE.g. "eng 你好"'
        command[
            "help"
        ] = 'To show all command.\nOr show specific command.\nE.g. "help open"'
        command["hide"] = "To toggle the input method, i.e. hide/show."
        command["ip"] = "To show internal and external ip addresses."
        command["lock"] = "To lock the screen."
        command[
            "open"
        ] = 'To open an application(with parameter(s)).\nE.g. "open notepad"'
        command[
            "random"
        ] = 'To generate a random number between two numbers(included).\nE.g. "random 1 3" to generate 1/2/3.'
        command[
            "remember"
        ] = 'To link two strings(for open app).\nE.g. "remember chrome C:\\users\\tom\\desktop\\chrome.lnk"\nThen you can open it with command "open" or "run"\ni.e. "open chrome" or "run chrome"'
        command["restart"] = "To restart the computer."
        command[
            "run"
        ] = 'To run an application(with parameter(s)) in Tom.\nE.g. "run notepad"'
        command["shutdown"] = "To shutdown the computer."
        command["version"] = "To see the version of the program and check for update."
        command["weather"] = "To show current weather information."
        command["where"] = 'To show the location of command.\nE.g. "where notepad"'
        command["wiki"] = 'To get information from wikipedia.\nE.g. "wiki python"'

        com = com.lower()

        if com in command:
            re += f'\nUsage of "{com}":\n\n{command[com]}'
        else:
            re = f'Command not found - "{com}"'
    return re


def run(para):
    if para[0] == "help":
        # show all commands
        if len(para) == 1:
            say('Enter "help <command>" to learn the specific command.')
            print()
            say(f"Command list: {commandInfo()}")
        else:
            # specific command
            say(commandInfo(para[1]))
    elif para[0] == "clear":
        if WINDOWS:
            os.system("cls")
        else:
            os.system("clear")
        say(f"Hello, {data['config']['username']}. What can I help you?")
    elif para[0] == "random":
        if len(para) < 3:  # check if there are enough parameters
            say("Please tell me two numbers.")
        else:
            try:  # check if two parameters are integers
                n1 = int(para[1])
                n2 = int(para[2])
            except:
                say("Two numbers must be integer.")
            else:
                if n1 < n2:  # n2 may bigger than n1
                    i = random.randint(n1, n2)
                else:
                    i = random.randint(n2, n1)
                say(i)  # show the generated integer
    elif para[0] == "remember":
        if len(para) < 3:
            say("Please tell me one name and one location.")
        else:
            temp = " ".join(para[2:])  # ignore the command and first parameter(name)
            data["remember"][para[1]] = temp
            writeData()
            say("Done!")
    elif para[0] == "open":
        if len(para) < 2:
            say("What do you want to open? Please tell me as well.")
        else:
            if para[1] in data["remember"]:
                para[1] = data["remember"][para[1]]  # now para[1] contains the location

            temp = " ".join(para[1:])

            say(f"Trying to open {temp}!")

            dirName = os.path.dirname(temp)
            if len(dirName) >= 1:
                dirNames = " ".join(dirName.split(" ")).split(" ")

                pathExist = False
                for name in dirNames:
                    if name[0] == '"' and name.count('"') % 2 == 1:
                        name = name[1:]
                    elif name[0] == "'" and name.count("'") % 2 == 1:
                        name = name[1:]
                    if os.path.exists(name) and os.path.isdir(name):
                        dirName = name
                        pathExist = True
                        break
                if WINDOWS:
                    if pathExist:
                        os.system(f'start "" /D {dirName} {temp}')
                    else:
                        os.system(f'start "" {temp}')
                else:
                    # non windows
                    if pathExist:
                        os.system(f"cd {dirName} && {temp} & disown")
                    else:
                        os.system(f"{temp} & disown")
            else:
                if WINDOWS:
                    os.system(f'start "" {temp}')
                else:
                    os.system(f"{temp} & disown")
    elif para[0] == "run":
        if len(para) < 2:
            say("What do you want to run? Please tell me as well.")
        else:
            if para[1] in data["remember"]:
                para[1] = data["remember"][para[1]]  # now para[1] contains the location

            temp = " ".join(para[1:])

            dirName = os.path.dirname(temp)

            if len(dirName) >= 1:
                dirNames = " ".join(dirName.split(" ")).split(" ")

                pathExist = False
                for name in dirNames:
                    if name[0] == '"' and name.count('"') % 2 == 1:
                        name = name[1:]
                    elif name[0] == "'" and name.count("'") % 2 == 1:
                        name = name[1:]
                    if os.path.exists(name) and os.path.isdir(name):
                        dirName = name
                        pathExist = True
                        break
                if WINDOWS:
                    if pathExist:
                        os.system(f"Pushd {dirName} && call {temp} && popd")
                    else:
                        os.system(f"call {temp}")
                else:
                    # non windows
                    if pathExist:
                        os.system(f"cd {dirName} && {temp}")
                    else:
                        os.system(temp)
            else:
                if WINDOWS:
                    os.system(f"call {temp}")
                else:
                    os.system(temp)
    elif para[0] == "chinese" or para[0] == "chi":
        if len(para) < 2:
            say("What do you want to translate? Please tell me as well.")
        else:
            temp = " ".join(para[1:])

            say(translate(temp, "zh-TW"))
    elif para[0] == "english" or para[0] == "eng":
        if len(para) < 2:
            say("What do you want to translate? Please tell me as well.")
        else:
            temp = " ".join(para[1:])

            say(translate(temp, "en"))
    elif para[0] == "config":
        if len(para) == 1:
            say(
                'Usage: "config <x> <value>" to set value of x,\n"config <x>" to get the value of x,\nwhere x can be:\n\n-----'
            )
            for key in data["config"].keys():
                print(key)
            print("-----\n")
        elif len(para) == 2:
            try:
                say(f"Value of {para[1]}: {data['config'][para[1]]}")
            except:
                say('Unrecognized config!\nType "config" to know more.')
        else:  # len(para)>=3
            if para[1] in data["config"]:
                temp = " ".join(para[2:])
                data["config"][para[1]] = temp
                writeData()
                say("Success!")
            else:
                say('Unrecognized config!\nType "config" to know more.')
    elif para[0] == "weather":
        say(weather())
    elif para[0] == "ip":
        say(ip())
    elif para[0] == "hide":
        changed = False
        if len(para) == 1:
            data["private_config"]["hide_input"] = not data["private_config"][
                "hide_input"
            ]
            changed = True
        else:
            if para[1].lower() in ["true", "on"]:
                data["private_config"]["hide_input"] = True
                changed = True
            elif para[1].lower() in ["false", "off"]:
                data["private_config"]["hide_input"] = False
                changed = True
            else:
                say("Usage: hide [on|off]")

        if changed:
            if data["private_config"]["hide_input"]:
                say("Hide mode is now ON.")
            else:
                say("Hide mode is now OFF.")
            # save changes
            writeData()
    elif para[0] == "version":
        print(f"Your version: {VERSION}")
        currentVersion = getCurrentVersion()
        if VERSION == currentVersion:
            print("Tom is up to date.")
        else:
            if currentVersion is None:
                print(f"For more information, please visit: {RELEASE_LINK}")
            else:
                print(f"Latest version: {currentVersion}")
                print(f"To update, please visit: {RELEASE_LINK}")
            pyperclip.copy(RELEASE_LINK)
            print("The link has been copied to the clipboard.")
    elif para[0] == "lock":
        # lock screen
        if WINDOWS:
            os.system("rundll32.exe user32.dll, LockWorkStation")
        else:
            os.system("systemctl suspend")
        say("Trying to lock screen.")
    elif para[0] == "shutdown":
        # shutdown
        if WINDOWS:
            os.system('shutdown -s -t 0 -c " "')
        else:
            os.system("shutdown -h 0")
        say("Trying to shutdown the computer.")
    elif para[0] == "restart":
        # restart
        if WINDOWS:
            os.system('shutdown -r -t 0 -c " "')
        else:
            os.system("reboot")
        say("Trying to restart the computer.")
    elif para[0] == "wiki":
        if len(para) < 2:
            say("What do you want to search? Please tell me as well.")
        else:
            temp = " ".join(para[1:])

            try:
                say(translate2(wikipedia.summary(temp)))
            except:
                say("Cannot find the page.")
    elif para[0] == "where":
        if len(para) < 2:
            say("Please include what you want to search.")
        else:
            if para[1] in data["remember"]:
                say(data["remember"][para[1]])
            else:
                say("Not found.")
    else:
        say('Unrecognized command!\nType "help" to see the command list.')

    if WINDOWS:
        # change console title every time in case the title has been changed.
        os.system(f"title {NAME}")


readData()


if WINDOWS:
    os.system(f"title {NAME}")  # change console title


if len(sys.argv) > 1:
    # run Tom with parameter(s)
    ans = " ".join(sys.argv[1:])

    if ans.replace(" ", "") != "":
        # not empty input
        para = parse(ans)
        if len(para) >= 1:
            run(para)

    # exit the program
    sys.exit(0)


say(f"Hello, {data['config']['username']}. What can I help you?")

print()

if data["private_config"]["hide_input"]:
    ans = getpass.getpass("You: ")
else:
    ans = input("You: ")

print()

while ans.lower() != "exit" and ans.lower() != "bye" and ans.lower() != "quit":
    if WINDOWS:
        # change console title every time in case the title has been changed.
        os.system(f"title {NAME}")

    if ans.replace(" ", "") != "":
        # not empty input
        para = parse(ans)

        if len(para) >= 1:
            run(para)

        print()

    if data["private_config"]["hide_input"]:
        ans = getpass.getpass("You: ")
    else:
        ans = input("You: ")
    print()


# exit
say("Bye bye!")
