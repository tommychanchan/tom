@echo off

title build exe

set appName="Tom"

:: delete old exe
del /f /s /q %appName%".exe"

:: build exe
python -m PyInstaller -F main.py -i favicon.ico --onefile --name %appName%

:: copy exe from /dist to root folder and rename it
copy /Y "dist" "%cd%"

:: delete cache files
del /f /s /q "__pycache__\*.*"
del /f /s /q "build\*.*"
del /f /s /q "dist\*.*"
del /f /s /q "*.spec"

:: delete chche folders
rmdir /Q /S "__pycache__"
rmdir /Q /S "build"
rmdir /Q /S "dist"