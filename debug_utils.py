import time
import functools


def trace(func):
    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        print(f"Trace: Call function {func.__name__}, {args=} {kwargs=}")
        result = func(*args, **kwargs)
        print(f"Trace: Function {func.__name__} return {result!r}")
        return result

    return wrapper


def timer(func):
    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        start = time.perf_counter()
        result = func(*args, **kwargs)
        end = time.perf_counter()
        run_time = end - start
        print(f"Timer: Function {func.__name__} used {run_time} seconds")
        return result

    return wrapper
